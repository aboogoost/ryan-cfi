<!DOCTYPE HTML>
<!--
	Landed by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		@include('components.head', ['pageTitle' => 'Tools'])
	</head>
	<body class="is-preload">
		<div id="page-wrapper">

			@include('components.header')

			<!-- Main -->
				<div id="main" class="wrapper style1">
					<div class="container">
						<header class="major">
							<h2>Tools</h2>
							<p>Use these to assist with your flight training.</p>
						</header>

						<!-- Slant Alpha -->
						<div class="row gtr-150">
							<!-- Image -->
							<div class="col-8 col-12-medium imp-medium">

								<section id="content">
									<a href="/SlantAlpha" class="image fit"><img src="img/slant_alpha_screenshot.png" alt="" /></a>
								</section>
							</div>

							<!-- Description -->
							<div class="col-4 col-12-medium">
								<section id="sidebar">
									<h3>Slant Alpha</h3>
									<p>I created this simple flight simulator to assist in practicing VOR navigation, and holding.</p>
									<p>Includes a simple mode for getting started as well as a realistic mode that uses FAA data.</p>
									<footer>
										<ul class="actions">
											<li><a href="/SlantAlpha" class="button">GO</a></li>
										</ul>
									</footer>
								</section>
							</div>
						</div>

						<!-- Hold -->
						<div class="row gtr-150">
							<!-- Image -->
							<div class="col-8 col-12-medium imp-medium">

								<section id="content">
									<a href="/SlantAlpha?s=hold" class="image fit"><img src="img/hold_screenshot.png" alt="" /></a>
								</section>
							</div>

							<!-- Description -->
							<div class="col-4 col-12-medium">
								<section id="sidebar">
									<h3>Hold Practice</h3>
									<p>Slant Alpha level built for practicing holding procedures.</p>
									<footer>
										<ul class="actions">
											<li><a href="/SlantAlpha?s=hold" class="button">GO</a></li>
										</ul>
									</footer>
								</section>
							</div>
						</div>

						<!-- Demo -->
						<div class="row gtr-150">
							<!-- Image -->
							<div class="col-8 col-12-medium imp-medium">

								<section id="content">
									<a href="/SlantAlpha?s=simple" class="image fit"><img src="img/vor_screenshot.png" alt="" /></a>
								</section>
							</div>

							<!-- Description -->
							<div class="col-4 col-12-medium">
								<section id="sidebar">
									<h3>VOR Demonstration</h3>
									<p>A simplified version of Slant Alpha designed to help flight instructors explain the basics of VOR navigation.</p>
									<footer>
										<ul class="actions">
											<li><a href="/SlantAlpha?s=simple" class="button">GO</a></li>
										</ul>
									</footer>
								</section>
							</div>
						</div>

						<!-- Decoder -->
						<div class="row gtr-150">
							<!-- Description -->
							<div class="col-12 col-12-medium">
								<section id="sidebar">
									<h3>Decoder</h3>
									<p>A list of abbreviations from the <a href="https://www.faa.gov/air_traffic/publications/atpubs/cnt_html/chap2_section_1.html" target="_blank" rel="noopener noreferrer">FAA</a>. Use to help in decoding METARs, TAFs, NOTAMs, etc.</p>
									<p>I recommend adding the PDF to Foreflight so you have it handy.</p>
									<footer>
										<ul class="actions">
											<li><a download href="/files/Decoder.pdf" class="button">Download PDF</a></li>
										</ul>
									</footer>
								</section>
							</div>
						</div>

					</div>
				</div>

			@include('components.footer')

		</div>

		@include('components.scripts')

	</body>
</html>