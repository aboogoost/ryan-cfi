<!-- Header -->
<header id="header">
	<h1 id="logo"><a href="/">Ryan CFI</a></h1>
	<nav id="nav">
		<ul>
			<li><a href="/tools">Tools</a></li>
			<li><a href="/contact" class="button primary">Contact</a></li>
		</ul>
	</nav>
</header>