<!DOCTYPE HTML>
<!--
	Landed by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
	<head>
		@include('components.head', ['pageTitle' => 'Contact'])
	</head>
	<body class="is-preload">
		<div id="page-wrapper">

			@include('components.header')

			<!-- Main -->
				<div id="main" class="wrapper style1">
					<div class="container">
						<header class="major">
							<h2>Ready to take your aviation journey to new heights?</h2>
							<p>Whether you're a beginner dreaming of taking flight or an experienced pilot seeking to expand your skills, I'm here to guide you on an exciting path of discovery. If I'm not the best fit for your needs, I'll gladly recommend another resource. Share the details of your specific situation, and together, we'll tailor a personalized plan to meet your unique aviation goals.</p>
						</header>

						<!-- Form -->
							<section>
								<h3>Contact</h3>
								<form method="post" action="https://formspree.io/f/xknalyll">
									<div class="row gtr-uniform gtr-50">
										<div class="col-6">
											<input type="text" name="name" id="name" value="" placeholder="Name" />
										</div>
										<div class="col-6"></div>
										<div class="col-6 col-12-xsmall">
											<input type="email" name="email" id="email" value="" placeholder="Email" required />
										</div>
										<div class="col-6 col-12-xsmall">
											<input type="tel" name="phone" id="phone" value="" placeholder="Phone" />
										</div>
										<div class="col-12">
											<textarea name="message" id="message" placeholder="Enter your message" rows="6"></textarea>
										</div>
										<div class="col-12">
											<ul class="actions">
												<li><input type="submit" value="Send Message" class="primary" /></li>
											</ul>
										</div>
									</div>
								</form>
							</section>

					</div>
				</div>

			@include('components.footer')

		</div>

		@include('components.scripts')

	</body>
</html>